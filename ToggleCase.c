#include <stdio.h>
#include <string.h>

int main(){

	char *p;
	int len=0,i=0;

	p = (char *)malloc(sizeof(char)*1000);

	printf("Enter a string\n");
	gets(p);

	len = strlen(p);

	for (i = 0; i < len; i++) {
		if (p[i] >= 65 && p[i] <= 90)
				p[i] += 32;
		else if(p[i] >= 97 && p[i] <= 122)
			p[i] -= 32;
	}

	printf("\nThe Toggled Case Sensitive String is \n");
	puts(p);
	free(p);
	return 0;
}
