# Practice-Codes
Simple codes created for practice in C. All the codes have been compiled and executed successfully on gcc version 4.8.2 (Ubuntu 4.8.2-19ubuntu1).

These are some of the simple codes created by me while I was practicing. The codes are categorized as follows.

NUMBERS:

1. Check if a given number is a Palindrome or not without using additional space.
2. Print "Number of digits in a given number". (upto 2048 digits, can be extended further)
3. Uniquely Reversing a Number - Reverses the given number in two ways. (a. No of digits are provided by the user.   b) System computes itself)

LINKED LIST:

1. Print the Linked List in reverse order. (Works with numbers only)
2. Linked List Implementation. (Push, Pop, Display and Exit operations.)

STRINGS:

1. Sentence Reversal Program - Prints the reverse of a Sentence. (Ex: I/P = He is Good, O/P = Good is He)
2. Toggle Case - This program inverts the case of the strings given as Input.


While compiling certain codes in gcc, please use "-lm" as these codes have used in-built math functions.
